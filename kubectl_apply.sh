#!/bin/bash
export imgdate=$(git log -1 --format="%at" | xargs -I{} date -d @{} +%y%m%d)
export imgsha=$(git rev-parse HEAD | cut -c1-8)
export ci_commit_branch=$CI_COMMIT_BRANCH

#kubectl create namespace ${CI_COMMIT_BRANCH}
#echo "Creating Namespace "${CI_COMMIT_BRANCH}
#kubectl apply -f ./kubernetes/Check-Database.yaml -n ${CI_COMMIT_BRANCH}
#echo "Creating Configmap"
#kubectl apply -f ./kubernetes/dbsecret.yaml -n ${CI_COMMIT_BRANCH}
#echo "Adding Database Sercet to Cluster"
#kubectl apply -f mysql-statefulset.yaml -n ${CI_COMMIT_BRANCH}
#echo "Creating Mysql Statefulser pod in Namespace"
#kubectl apply -f mysql-service.yaml -n ${CI_COMMIT_BRANCH}
#echo "Creating Mysql Service"
#kubectl apply -f devops-service.yaml -n ${CI_COMMIT_BRANCH}
#echo "Creating Application service"
#envsubst < devops-deployment.yaml | kubectl apply -n ${CI_COMMIT_BRANCH} -f -
#echo "Creating Application pod"

envsubst < ./kubernetes/namespace.yaml | kubectl apply -n ${CI_COMMIT_BRANCH} -f -

#kubectl apply -f ./kubernetes/namespace.yaml -n ${ci_commit_branch}
envsubst < ./kubernetes/devops-deployment.yaml | kubectl apply -n ${CI_COMMIT_BRANCH} -f ./kubernetes
envsubst < ./kubernetes/devops-deployment.yaml | kubectl apply -n ${CI_COMMIT_BRANCH} -f -

helm upgrade --install ingress-nginx ./cicd/my-helm/charts/nginx-ingress/ --set CI_COMMIT_BRANCH=$ci_commit_branch
echo "Installing Ingress Nginx Controller"

#kubectl create secret tls certificate --key tls.key --cert tls.crt -n ${CI_COMMIT_BRANCH}
#echo "Adding TLS Secret to be used with Ingress"
#kubectl apply -f ingress.yaml -n ${CI_COMMIT_BRANCH}
#echo "Exposing Application Service Using Ingress"
echo ---------------------------------------------------------
helm upgrade --install mysql ./cicd/my-helm/charts/mysql --namespace ${CI_COMMIT_BRANCH}