use the follwoing commands to get your URLs:
- Kibana:
echo "your kibana url is https://"$(kubectl get svc | grep quickstart-kb-http | awk '{print $3}')":5601"

- Prometheus:
echo "your prometheus url is http://"$(kubectl get svc --all-namespaces | grep prometheus-k8s | awk '{print $4}')":9090"

or
https://prometheus.progressoft.dev/

- Grafana:
echo "your grafana url is http://"$(kubectl get svc -n monitoring | grep grafana | awk '{print $3}')":3000"

or
https://grafana.progressoft.dev/

- Application:
echo "your application url is http://"$(kubectl get svc --all-namespaces | grep 8090 | awk '{print $4}')":8090"

or 

https://devops.progressoft.dev

- Application Ingress Service:
echo "your application ingress service url is https://"$(kubectl get svc --all-namespaces | grep ingress-controller | awk '{print $5}')""

---------------------------------

